import { MouseEvent, useState, useEffect } from 'react';

import ReactFlow, {
  ReactFlowProvider,
  Background,
  BackgroundVariant,
  Node,
  Edge,
  useReactFlow,
  useNodesState,
  useEdgesState
} from 'react-flow-renderer';

const onNodeDragStop = (_: MouseEvent, node: Node) => console.log('drag stop', node);
const onNodeClick = (_: MouseEvent, node: Node) => console.log('click', node);

// TODO: onClick make changes in the state (create a new node connected to the parent node,
// with the beliefs of the agent)
// Missing: actions as consistent with goals
/*
const complexLabel = <>
    <div style={{ backgroundColor:'magenta' }}> <i>believes</i><sub>CHINIRA</sub> (has(boyang,knife)) </div>
    <div> <span style={{ fontVariantCaps: 'small-caps' }}>Goal</span> (<i>believes</i><sub>CHINIRA</sub> (has(boyang,knife))) </div>
    <div className="proposition1"> "c" </div>
  </>;
*/


function SomeBelief() {
  const [divColor, setDivcolor] = useState(['magenta', 'blue']);
  function getDivColor() {
    var color1, color2;
    [color1, color2] = divColor;
    return { backgroundColor:color1 }
  } 
  
  function toggleDivColor() {
    var color1, color2;
    [color1, color2] = divColor;
    setDivcolor([color2, color1]);
  }
  
  return (<div style={getDivColor()} onClick={toggleDivColor}>
    <i>believes</i><sub>CHINIRA</sub> (has(boyang,knife))
  </div>)
}

function GoalBelief() {
  return (
    <div> <span style={{ fontVariantCaps: 'small-caps' }}>Goal</span> (<i>believes</i><sub>CHINIRA</sub> (has(boyang,knife))) </div>
  )
}

/*
    <div style={{ backgroundColor:'magenta' }}> <i>believes</i><sub>CHINIRA</sub> (has(boyang,knife)) </div>
*/

type Belief = JSX.Element; //HTMLFactory<HTMLDivElement>;
type Fact = JSX.Element; //HTMLFactory<HTMLDivElement>;
type Goal = JSX.Element; //HTMLFactory<HTMLDivElement>;

type PlausBelief = {
  plaus: number,
  belief: Belief
}

type Frame = {
  tag: string;
  time: string;
  agent: string;
  beliefs: PlausBelief[];
  goals: Goal[];
  history: HistoryItem[];
}

type HistoryItem = {
  at: string,
  what: [Fact]
}

function renderPlausBelief(plBel: PlausBelief) {
  return <tr>
      <td>
    {plBel.plaus}
    </td>
  <td>
    {plBel.belief}
  </td>
   
  </tr>
}


function beliefSet1() : PlausBelief[] { 
  return [{plaus: 1, belief: SomeBelief()},
  {plaus: 7, belief: GoalBelief()}];
};

function frame_23_03_2022_09_00_aisha() : Frame {
  return {
  tag: '',
  time: '',
  agent: '',
  beliefs: beliefSet1(),
  goals: [],
  history: [] }
}

function renderFrame(frame: Frame) {return <>
  <div className='section-label'>
    {frame.time} - {frame.agent}
  </div>
  <hr></hr>
  <div className='section-label'>
    Beliefs
  </div>
  <div className='belief-box'>
    <table>
      <tr>
        <th>Plaus</th>
        <th>Belief</th>
      </tr>
      {frame.beliefs.map(renderPlausBelief)}
    </table>
  </div>
  <div className='section-label'>
    Goals
  </div>
  <div className='goal-box'>
  <table>
      <tr>
        <td>
          <div> <i>believes</i><sub>CHINIRA</sub> (has(boyang,knife)) </div>
        </td>
      </tr>
    </table>
  </div>
  <div className='section-label'>
    History
  </div>
  <div className='fact-box'>
    <table>
      <tr>
        <td>The Councillor...</td>
      </tr>
      <tr>
        <td>Somebody...</td>
      </tr>
    </table>
  </div>
</>};

function id_23_03_2022_09_00() {
  return renderFrame(frame_23_03_2022_09_00_aisha());
}

const id_23_03_2022_09_00_backup = <>
    <div className='section-label'>
      23rd March 09:00
    </div>
    <hr></hr>
    <div className='section-label'>
      Beliefs
    </div>
    <div className='belief-box'>
      <table>
        <tr>
          <th>Plaus</th>
          <th>Belief</th>
        </tr>
        <tr>
          <td>
            1
          </td>
          <td>
            {SomeBelief}
          </td>
        </tr>
        <tr>
          <td>
            7
          </td>
          <td>
            <div> <span style={{ fontVariantCaps: 'small-caps' }}>Goal</span> (<i>believes</i><sub>CHINIRA</sub> (has(boyang,knife))) </div>
          </td>
        </tr>
      </table>
    </div>
    <div className='section-label'>
      Goals
    </div>
    <div className='goal-box'>
    <table>
        <tr>
          <td>
            <div> <i>believes</i><sub>CHINIRA</sub> (has(boyang,knife)) </div>
          </td>
        </tr>
      </table>
    </div>
    <div className='section-label'>
      History
    </div>
    <div className='fact-box'>
      <table>
        <tr>
          <td>The Guildmistress...</td>
        </tr>
        <tr>
          <td>The Priest...</td>
        </tr>
      </table>
    </div>
  </>;

const id_16_03_2022_09_00 = <>
    <div className='section-label'>
      16th March 09:00
    </div>
    <hr></hr>
    <div className='section-label'>
      Beliefs
    </div>
    <div className='belief-box'>
      <table>
        <tr>
          <th>Plaus</th>
          <th>Belief</th>
        </tr>
        <tr>
          <td>
            1
          </td>
          <td>
            <SomeBelief/>
          </td>
           
        </tr>
        <tr>
          <td>
            7
          </td>
          <td>
            <div> <span style={{ fontVariantCaps: 'small-caps' }}>Goal</span> (<i>believes</i><sub>CHINIRA</sub> (has(boyang,knife))) </div>
          </td>
        </tr>
      </table>
    </div>
    <div className='section-label'>
      Goals
    </div>
    <div className='goal-box'>
    <table>
        <tr>
          <td>
            <div> <i>believes</i><sub>CHINIRA</sub> (has(boyang,knife)) </div>
          </td>
        </tr>
      </table>
    </div>
    <div className='section-label'>
      History
    </div>
    <div className='fact-box'>
      <table>
        <tr>
          <td>The Councillor...</td>
        </tr>
        <tr>
          <td>Somebody...</td>
        </tr>
      </table>
    </div>
  </>;

function id_16_03_2022_09_00_agt(agent) {return <>
    <div className='section-label'>
      16th March 09:00 - {agent}
    </div>
    <hr></hr>
    <div className='section-label'>
      Beliefs
    </div>
    <div className='belief-box'>
      <table>
        <tr>
          <th>Plaus</th>
          <th>Belief</th>
        </tr>
        <tr>
          <td>
            1
          </td>
          <td>
            <SomeBelief/>
          </td>
           
        </tr>
        <tr>
          <td>
            7
          </td>
          <td>
            <div> <span style={{ fontVariantCaps: 'small-caps' }}>Goal</span> (<i>believes</i><sub>CHINIRA</sub> (has(boyang,knife))) </div>
          </td>
        </tr>
      </table>
    </div>
    <div className='section-label'>
      Goals
    </div>
    <div className='goal-box'>
    <table>
        <tr>
          <td>
            <div> <i>believes</i><sub>CHINIRA</sub> (has(boyang,knife)) </div>
          </td>
        </tr>
      </table>
    </div>
    <div className='section-label'>
      History
    </div>
    <div className='fact-box'>
      <table>
        <tr>
          <td>The Councillor...</td>
        </tr>
        <tr>
          <td>Somebody...</td>
        </tr>
      </table>
    </div>
  </>};

const initialNodes: Node[] = [
  { id: 'id_16_03_2022_09_00',
    data: { label: id_16_03_2022_09_00 },
    position: { x: 100, y: 450 },
    style: {width: 250},
    className: 'light' },
  { id: 'id_23_03_2022_09_00',
    data: { label: id_23_03_2022_09_00() },
    position: { x: 100, y: 50 },
    style: {width: 250},
    className: 'light' },
];


/*
, style: {
    background: '#D6D5E6',
    color: '#333',
    border: '1px solid #222138',
    width: 250,
  }
*/

const initialEdges: Edge[] = [
  { id: '1',
    target: 'id_16_03_2022_09_00',
    source: 'id_23_03_2022_09_00',
    animated: false },
];

const BasicFlow = () => {
  const reactFlowInstance = useReactFlow();

/*
reactFlowInstance.addNodes(id_16_03_2022_09_00_agt(agent));
reactFlowInstance.addEdges(...);
*/
  const updatePos = () => {
    reactFlowInstance.setNodes((nodes) =>
      nodes.map((node) => {
        node.position = {
          x: Math.random() * 400,
          y: Math.random() * 400,
        };

        return node;
      })
    );
  };

  const logToObject = () => console.log(reactFlowInstance.toObject());
  const resetTransform = () => reactFlowInstance.setViewport({ x: 0, y: 0, zoom: 1 });

  const toggleClassnames = () => {
    reactFlowInstance.setNodes((nodes) =>
      nodes.map((node) => {
        node.className = node.className === 'light' ? 'dark' : 'light';

        return node;
      })
    );
  };

  // Otherwise:
  // "The React Flow parent container needs a width and a height to render the graph."
  const graphStyles = { width: "100%", height: "500px" };

  return (
    // TODO add updatable state!!!
/*
    const [nodes, setNodes, onNodesChange] = useNodesState(initialNodes);
    const [edges, setEdges, onEdgesChange] = useEdgesState(initialEdges);
  
    const [nodeName, setNodeName] = useState('Node 1');
    const [nodeBg, setNodeBg] = useState('#eee');
    const [nodeHidden, setNodeHidden] = useState(false);
  */
  
    <ReactFlow
      defaultNodes={initialNodes}
      defaultEdges={initialEdges}
      onNodeClick={onNodeClick}
      onNodeDragStop={onNodeDragStop}
      className="react-flow-basic-example"
      attributionPosition="top-center"
      defaultZoom={1.5}
      minZoom={0.2}
      maxZoom={4}
      style={graphStyles}
      fitView
    >
      <Background variant={BackgroundVariant.Lines} />

      <div style={{ position: 'absolute', right: 10, top: 10, zIndex: 4 }}>
        <button onClick={resetTransform} style={{ marginRight: 5 }}>
          reset transform
        </button>
        <button onClick={updatePos} style={{ marginRight: 5 }}>
          change pos
        </button>
        <button onClick={toggleClassnames} style={{ marginRight: 5 }}>
          toggle classnames
        </button>
        <button onClick={logToObject}>toObject</button>
      </div>
    </ReactFlow>
  );
};

/*
const onInit = (reactFlowInstance) => console.log('flow loaded:', reactFlowInstance);

const OverviewFlow = () => {
  const [nodes, setNodes, onNodesChange] = useNodesState(initialNodes);
  const [edges, setEdges, onEdgesChange] = useEdgesState(initialEdges);
  const onConnect = (params) => setEdges((eds) => addEdge(params, eds));

  return (
    <ReactFlow
      nodes={nodes}
      edges={edges}
      onNodesChange={onNodesChange}
      onEdgesChange={onEdgesChange}
      onConnect={onConnect}
      onInit={onInit}
      style={graphStyles}
      fitView
      attributionPosition="top-center"
    >
      <MiniMap
        nodeStrokeColor={(n) => {
          if (n.style?.background) return n.style.background;
          if (n.type === 'input') return '#0041d0';
          if (n.type === 'output') return '#ff0072';
          if (n.type === 'default') return '#1a192b';

          return '#eee';
        }}
        nodeColor={(n) => {
          if (n.style?.background) return n.style.background;

          return '#fff';
        }}
        nodeBorderRadius={2}
      />
      <Controls />
      <Background color="#aaa" gap={16} />
    </ReactFlow>
  );
};


export default OverviewFlow;
*/

export default function App() {
  return (
    <ReactFlowProvider >
      <BasicFlow />
    </ReactFlowProvider>
  );
}